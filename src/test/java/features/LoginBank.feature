Feature: Guru99 Bank Test Login Page Scenario Verification

#  Scenario Outline: To Verify Login Test Cases
#    Given Initialize the browser with Firefox
#    And Navigate to Demo Guru99 website
#    When Enter "<UserId>" And "<Password>"
#    Then Verify Login to the Application "Welcome To Manager's Page of Guru99 Bank"
#    Examples:
#      |   UserId       | Password   |
#      |   mngr273168d  |  ryguqAn   |
#      |   mngr1236     |  1236      |
#      |   mngr123ee456 |  ryguqAn   |
#      |   mngr123456   |  1236      |

  Scenario: To Verify Login Test Cases with Positive Scenario
    Given Initialize the browser with Firefox
    And Navigate to Demo Guru99 website
    When Enter "mngr273168" And "ryguqAn"
    Then Verify Login to the Application "Welcome To Manager's Page of Guru99 Bank"

  Scenario: To Verify Login Test Cases When userId is valid and password is Invalid
    Given Initialize the browser with Firefox
    And Navigate to Demo Guru99 website
    When Enter "mngr273168" And "1236"
    Then Verify Alert message "User or Password is not valid"
    And Close Browser

  Scenario: To Verify Login Test Cases When userId is Invalid and password is valid
    Given Initialize the browser with Firefox
    And Navigate to Demo Guru99 website
    When Enter "mngr1234" And "ryguqAn"
    Then Verify Alert message "User or Password is not valid"

  Scenario: To Verify Login Test Cases When userId is Invalid and password is Invalid
    Given Initialize the browser with Firefox
    And Navigate to Demo Guru99 website
    When Enter "mngr1234" And "1236"
    Then Verify Alert message "User or Password is not valid"
