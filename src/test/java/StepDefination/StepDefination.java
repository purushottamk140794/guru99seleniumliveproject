package StepDefination;

import PageObject.LoginPage;
import PageObject.ManagerHomePage;
import Resources.Utility;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

public class StepDefination extends Utility {
    WebDriver driver;
  @Given("^Initialize the browser with Firefox$")
  public void initialize_the_browser_with_firefox() throws Throwable {
      System.out.println("Driver is Initialize");
      driver = initializeBrowser();
  }
    @And("^Navigate to Demo Guru99 website$")

          public void navigate_to_URL() throws IOException {
          driver.get(getURL());
     }

     @When("Enter {string} And {string}")
     public void enter_And(String Username, String Password) {
         LoginPage loginPage=new LoginPage(driver);
         loginPage.getuserId().sendKeys(Username);
         loginPage.getpassword().sendKeys(Password);
         loginPage.getSubmit();
     }
    @Then("Verify Login to the Application {string}")

     public void verify_Login_to_the_Application_Successfully(String status) {
        WebDriverWait webDriverWait=new WebDriverWait(driver,3);
        WebElement guru99homepage=webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//marquee[@class='heading3']")));
        System.out.println("Verified "+guru99homepage.getText());
         Assert.assertEquals(status,guru99homepage.getText());
         driver.quit();
     }
    @Then("^Verify Alert message \"([^\"]*)\"$")
    public void verify_alert_message_something(String expectedAlertMessage) throws Throwable {

      String actualMessage=driver.switchTo().alert().getText();
      driver.switchTo().alert().accept();
      Assert.assertEquals(expectedAlertMessage,actualMessage);
    }

    @Then("^Verify If managerId is Matches \"([^\"]*)\" And take the Screenshot$")
    public void verify_if_managerid_is_matches_something(String managerId) throws Throwable {
        ManagerHomePage mhp=new ManagerHomePage(driver);
        String combinedText= mhp.getmanagerId().getText();
        String actualManagerId=combinedText.split(":")[1];
        System.out.println(combinedText);
        System.out.println(actualManagerId);
        Assert.assertEquals(actualManagerId.trim(),managerId);
        getScreenShot(driver);
    }

    @And("Close Browser")
    public void closeBrowser()
    {
        driver.close();
    }

 }
