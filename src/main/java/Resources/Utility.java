package Resources;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

  public class Utility {
    Properties properties=new Properties();
    public WebDriver driver;
    public WebDriver initializeBrowser() throws IOException {
        properties = new Properties();
        FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"//src//main//java//Resources//Global.properties");
        properties.load(fis);
        String browserName=properties.getProperty("Browser");
        System.out.println("Browser Name "+browserName);
        if(browserName.equalsIgnoreCase("chrome"))
        {
            System.setProperty("webdriver.crome.driver", System.getProperty("user.dir") + "//src//main//java//resources//chromedriver.exe");
            driver = new ChromeDriver();
        }
        else if (browserName.contains("Firefox")) {
            System.out.println("Firefox");
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//src//main//java//Resources//geckodriver.exe");
            FirefoxOptions options = new FirefoxOptions();
            if (browserName.contains("headless"))
            {
                options.addArguments("--headless");
            }
            driver = new FirefoxDriver();
        } else if (browserName.equalsIgnoreCase("IE")) {
            System.setProperty("webdriver.IE.driver", System.getProperty("user.dir") + "//src//main//java//resources//IEdriver.exe");
            driver = new InternetExplorerDriver();
        }
        return driver;
    }
    public String getURL() throws IOException {
        properties = new Properties();
        FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"//src//main//java//Resources//Global.properties");
        properties.load(fis);
        return properties.getProperty("baseURL");


    }
    public void getScreenShot(WebDriver driver) throws IOException {
        TakesScreenshot takesscreenshot=(TakesScreenshot) driver;
        File sourceOutput= takesscreenshot.getScreenshotAs(OutputType.FILE);
        String destination = System.getProperty("user.dir") + "\\reports\\screenshot.png";
        FileUtils.copyFile(sourceOutput, new File(destination));
    }


}
