package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage  {
    WebDriver driver;
    By userId = By.name("uid");
    By password = By.name("password");
    By submit = By.name("btnLogin");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getuserId() {
        return driver.findElement(userId);

    }
    public WebElement getpassword() {
             return driver.findElement(password);
}
public LoginPage getSubmit()
{
    driver.findElement(submit).click();
    LoginPage loginPage=new LoginPage(driver);
    return loginPage;

}

}
