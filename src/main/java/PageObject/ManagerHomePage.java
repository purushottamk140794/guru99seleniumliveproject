package PageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManagerHomePage {
    WebDriver driver;
    By managerId = By.cssSelector(".heading3 td:nth-child(1)");


    public ManagerHomePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getmanagerId() {
        return driver.findElement(managerId);

    }
}
